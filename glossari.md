# SISTEMA ACME - GLOSSARI #

- **Banc de temps**: és un sistema d'intercanvi de serveis per temps. La unitat d'intercanvi no son diners, sinó temps.
- **Escalabilitat**: capacitat de crèixer sense degradar el servei oferit.
- **Moneda-temps**: moneda virtual que utilitzarem al projecte pels intercanvis.
- **Time4Time**: nom del projecte.
- **Review**: avaluació pública d'un servei o part d'un sistema qualificant-la.
