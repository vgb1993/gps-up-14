# SISTEMA ACME - PLA DE DESENVOLUPAMENT DE SOFTWARE #


## 1. ORGANITZACIÓ I EQUIP ##

> Organització i equip de treball:

- Cap de projecte: Serà l'encarregat de liderar l'equip i del business modeling. Només hi haura un cap.

- Arquitecte software: Serà l'encarregat d'analitzar els requeriments i dissenyar el software. Només hi haura un arquitecte.

- Desenvolupador senior: Serà l'encarregat de desenvolupar el software, implementar els dissenys de l'arquitecte i delegar funcions al junior. Transmetrà coneixements als juniors. Hi haura un desenvolupador senior.

- Desenvolupador junior: Els juniors faran la feina que els hi delegui el senior. També hauran d'assolir els coneixements que els hi transmet el senior. Hi haurà dos juniors

- Dissenyador gràfic: Serà l'encarregat del disseny gràfic de tota la interface d'usuari, de les animacions i transicións, de les campanyes de màrqueting i de la imatge corporativa de l'empresa. Hi haurà un disseyador.

## 2. ESTIMACIÓ D'ESFORÇ ##

> Calcular el nombre d'hores del projecte. Useu un excel o altra eina per calcular UCPs i convertir a hores. Copieu aquí les taules (però entregueu també l'excel!)

![](https://dl.dropboxusercontent.com/u/11779029/gps/01-esfor%C3%A7-projecte.png)
![](https://dl.dropboxusercontent.com/u/11779029/gps/02-esfor%C3%A7-treballadors.png)
![](https://dl.dropboxusercontent.com/u/11779029/gps/03-casos-dus.png)
![](https://dl.dropboxusercontent.com/u/11779029/gps/04-actors-externs.png)
![](https://dl.dropboxusercontent.com/u/11779029/gps/05-complexitat-tecnica.png)
![](https://dl.dropboxusercontent.com/u/11779029/gps/06-factors-entorn.png)
![](https://dl.dropboxusercontent.com/u/11779029/gps/10-cobertura-casos-dus.png)


## 3. ESTIMACIÓ DE COST ##

![](https://dl.dropboxusercontent.com/u/11779029/gps/07-rols.png)
![](https://dl.dropboxusercontent.com/u/11779029/gps/08-equip.png)
![](https://dl.dropboxusercontent.com/u/11779029/gps/09-altres-factors.png)



## 4. PLA DE PROJECTE ##


### Inception ###

> En aquesta primera fase, l’objectiu principal és entendre el que se’ns planteja. En aquest punt, el cap de projecte (businsess-modeling i gestor del projecte) junt amb l’arquitecte de software, són els protagonistes principals en aquesta fase.

> Les principals tasques a realitzar són les següents:

- Preparar l’entorn de treball del projecte
- Esbós el disseny de l’arquitectura del software
- Reconèixer els casos d’ús
- S’avaluen els riscos
- El model de negoci
- Avaluar la viabilitat
- Estimació de costos i planificació del temps

### Elaboration ###
> En aquesta fase, després d’haver analitzat els requeriments i entès el problema, l’arquitecte i el cap de projecte han de transmetre els requeriments i la proposta de solució a la resta de membres de l’equip.
> Les tasques a fer seran les següents:

- Acabar de definir el disseny de l’arquitectura de software.
- Considerar riscos del disseny.
- Acabar de polir la visió del projecte.
- Demostrar que l’arquitectura és escalable a un cost raonable.
- Definir un calendari per a la fase de construcció.

### Construction ###
> Es la fase més extensa on els desenvolupadors senior i junior s’encarregaran de acabar el desenvolupament del software. Es farà mitjançant iteracions on es aniran complint els casos d’ús, i en base a aquestes iteracions es podràn fer releases per que es pugui començar a fer testeig. El desenvolupador senior serà l’encarregat de assegurar la qualitat i assolir la màxima fidelitat al disseny gràfic presentat pel dissenyador. Les tasques son les seguents. 
	
- Desenvolupar els casos d’ús
- Acabar el software
- Començar el testing
- Polir el disseny

### Transition ###
> És la última fase és on es farà el deployment pels usuaris finals. S’avaluarà el feedback dels usuaris finals mitjançant releases inicials i comprovant el correcte funcionament. En aquesta part l’analista i el cap de projecte tenen un pes important perquè s’avalua si s’han aconseguit les metes marcades en la fase d’inception. En base a això es decideix si el projecte ja està llest per portar-lo al mercat. També serveix perquè els usuaris finals es familiaritzin amb la interfície i aprenguin a utilitzar-la.
