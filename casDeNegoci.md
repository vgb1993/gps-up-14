# SISTEMA ACME - CAS DE NEGOCI #


## 1. DESCRIPCIÓ DEL PRODUCTE ##

> Time4Time es una aplicació mòvil destinada a intercanviar temps per activitats o objectes. Els usuaris faràn ofertes de diferent tipus  per acumular temps. Un temps que haurà marcat el mateix usuari. Els demés usuaris podràn veure les diferents ofertes que hi han disponibles, i les podran adquirir només si acumulen sufucient temps.

## 2. CONTEXT DE NEGOCI ##

> El negoci és de l'àmbit dels serveis, intercanvi de productes i serveis. És un software de distribució gratuïta enfocat al públic general. Bàsicament tenim dos perfils d'usuaris, gent amb temps que vol oferir serveis, i gent amb o sense temps que en vol rebre. Té una vida prevista ilimitada donat el seu àmbit.

## 3. OBJECTIUS DEL PRODUCTE ##

1. *Objectiu 1*. Cobrir la demanda de serveis a canvi de temps. Actualment hi ha molta demanda de serveis de tot tipus, però donada la situació actual de crisis, i la inestabilitat del mercat, no tothom es pot permetre el que vol. Time4Time ajuda a reduir les barreres dels diners per aconseguir que qualsevol persona, pugui gaudir del servei que desitji. 
2. *Objectiu 2*. Millorar la red de bancs de temps físics. Time4Time aportarà una visió molt diferent als bancs de temps físics, aportant canvis que facilitin la gestió i desenvolupament dels serveis i els usuaris. Time4Time no tindrà cap cost, ni prerequisit, facilitant així que tothom pugui gaudir-ne del servei.
3. *Objectiu 3*. No restringir l'accès dels usuaris. Qualsevol usuari pot fer-ne ús. Time4Time preten reunir a diferents perfils d'usuari: gent amb temps disposada a donar serveis i gent sense temps que vol gaudir de certs serveis. Time4Time té més d'una manera de conseguir monedes-temps, fent possible que fins i tot la gent sense temps pugui accedir a serveis. 
4. *Objectiu 4*. Deslligar el servei de qualsevol lloc físic. Per a poder utilitzar Time4Time no es necessita anar a cap lloc físic, es pot fer servir des de qualsevol lloc on es disposi d'internet.
5. *Objectiu 5*. Disponibilitat ilimitada. El servei que ofereixen actualment altres competidors és limitat, donat que las seus físiques tenen un horari. Time4Time és una plataforma sense horaris, accessible en qualsevol moment.
6. *Objectiu 6*. Facilitar l'intercanvi de serveis i coneixements entre la població de manera transparent. Mai havia estat tant fàcil oferir/sol·licitar un servei, la nostra plataforma posarà en comú serveis de diferents àmbits perquè l'usuari pugui triar-ne quin desitja. No intervindrem de ninguna manera en els intercanvis de temps més enllà de controlar la legalitat de les ofertes dels nostres usuaris.
7. *Objectiu 7*. Serveis de qualitat. Time4Time no pot controlar ni jutger la qualitat dels serveis, és per això, que els propis usuaris seràn els que decidiran i puntuaran els serveis i usuaris amb els que hagin intercanviat objectes o serveis. Es podrà filtrar entre els serveis de major qualitat per poder obtenir el resultat esperat del servei.
8. *Objectiu 8*. Millorar la vida dels barcelonins. Volem incentivar el coneixement i els afers socials, promovent l'aprenentatge i la continua formació de la població de Barcelona en un àmbit col·laboratiu. Premiarem aquelles activitats/serveis que facin un ús responsable o un profit social per fomentar la millora de Barcelona.



## 4. RESTRICCIONS ##

1. *Restricció 1*. L'abast del projecte és la població de Barcelona. Una restricció inicial, de forma temporal, és que estem limitats a la població de Barcelona, així que no podem, actualment, rebre ofertes o usuaris de fora de Barcelona. És una restricció temporal que canviarà conforme la plataforma vagi creixent.
2. *Restricció 2*. S'ha de controlar l'inflació de la moneda-temps. Som conscients que això ens imposa una sèrie de restriccions derivades que haurem de controlar per no crear grans desbalancejos de moneda-temps entre usuaris.
3. *Restricció 3*. Inversió inicial. Estem restringits en el nombre de servidors i de recursos que podem assolir per una inversió inicial per començar a obrir el mercat i tenir moviment.
4. *Restricció 4*. Possibles usuaris. Donada la restricció 1, no podem assolir usuaris de tot arreu, i tampoc podem assolir usuaris que no tinguin temps ni objectes per intercanviar-los per serveis.
5. *Restricció 5*. Horaris i calendari de treball. Donat que hi ha dies festius i que l'horari dels treballadors no és 24h, hem d'adaptar les fases del projecte per poder dur-les a terme dins d'aquests horaris.
6. *Restricció 6*. Restriccions tecnològiques i de software. La nostra plataforma treballarà amb llenguatges/estàndars que implicaràn certes limitacions tecnològiques. Per accedir a la plataforma serà necessari una versiò mínima de software pels mòvils i una versió software mínima pels navegadors web per poder fer-ne ús d'una manera correcta. Necessitem garantir que l'usuari es de Barcelona i farem ús de serveis de geolocalització.


## 5. PREVISIÓ FINANCERA ##

> El producte no és per comercialitzar, és gratuït. Tot i així, els clients podran visualitzar anuncis per conseguir moneda-temps, i això, ens genera un benefici. El benefici es podria calcular: nombre de visualitzacions x preu/visualització. El sistema treballarà sobre servidors amazon escalables. Tindrem uns serveis mínims contractats sobre els quals treballarem i en moments pic, quan hi hagi molta càrrega en el servidor, es demanaràn més recursos al nostre proveïdor per tal d'assegurar un bon servei. Els beneficis vindran donats per el nombre de gent que visualitzi anuncis i les diferentes empreses que vulguin financiar/fer de sponsor publicitari amb menció a la web.

## 6. RECURSOS ##
[1] Web del banc de temps de sants: http://www.bancdetempsdesants.org/
[2] Banc de temps de gràcia: http://gracia.bdtonline.org/
[3] Informació sobre que son els bancs de temps http://xarxanet.org/comunitari/noticies/que-son-els-bancs-del-temps
[4] Informació adiconal sobre els bancs de tmeps http://www.barcelonesjove.net/area/la-teva-economia/bancs-dels-temps

