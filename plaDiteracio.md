# SISTEMA ACME - PLA D'ITERACIÓ INCEPTION #

## 1. PRESENTACIÓ DE LA ITERACIÓ ##

La fase d'inception tindrà una durada de 130 hores. En aquesta fase el esforç desglossat per rols serà el següent:
- Cap de projecte: 30%
- Arquitecte de software: 50%
- Desenvolupador senior: 5%
- Dissenyador gràfic: 15%

Com es pot observar el pes recaurà bàsicament en l'arquitecte de software i el cap de projecte, donat que és una iteració molt temprana on s'avaluen el bussines model, la possible arquitectura de sistema per poder-ne valorar la viabilitat i els riscos que existeixin. El dissenyador gràfic tindrà poc esforç donat que es realitzaran uns petits mockups inicialsi el desenvolupador començarà a esbossar els possibles casos d'ús.

En aquesta fase s'identificaran tots els casos d'ús que anirem tractant a les següents iteracions.

## 2. COBERTURA DE CASOS D'ÚS ##

En acabar la iteració la cobertura dels casos d'ús serà la següent:

Postejar una oferta: 								esbós
Esborrar una oferta: 								esbós
Llistat d'ofertes filtrades: 						identificat
Intercanvi d'ofertes: 								identificat
Marcar una oferta com a preferida					identificat
Publicar una review									identificat
Seguir a un usuari									identificat
Compartir a les xarxes socials						identificat
Convidar a altres persones a utilitzar Time4Time	identificat
Aconseguir temps a canvi d'anuncis					identificat
Login / registre									identificat

Com es pot observar s'ha fet uns primers esbossos i versions preliminars a mode de prova per veure com podria tracar-se el cas d'ús. La resta de casos d'ús s'han identificat per poder tractar-los en properes iteracions. Com podem veure no es cobreixen ni s'avancen gaire els casos d'úsdonat que el nivell d'esforç d'aquesta iteració és bastant baix respecte les altres iteracions.

## 3. ACTIVITATS ##

A - El model de negoci: 40 hores
B - S’avaluen els riscos: 9 hores
C - Avaluar la viabilitat: 19 hores
D - Reconèixer els casos d’ús: 26 hores
E - Estimació de costos i planificació del temps: 15 hores
F - Preparar l’entorn de treball del projecte: 5 hores
G - Esbós el disseny de l’arquitectura del software: 45 hores

Per fer les activitats B, D i F abans s'ha d'haver fet el model de negoci (A), i per fer la G i la E abans s'han d'haver reconegut els casos d'us (D). Per evaluar la viabilitat s'han d'haver evaluat els riscos.
 
## 4. DIAGRAMA DE GANTT ##

####Diagrama de Pert ####
Les unitats són en hores:
![](https://dl.dropboxusercontent.com/u/11779029/gps/pert.png)

####Diagrama de GANTT ####
Les unitats són en hores:
(El diagrama de Gantt està a un arixiu apart.)