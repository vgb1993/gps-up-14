# SISTEMA ACME - ESPECIFICACIÓ DE REQUISITS DEL SOFTWARE #

## 1. ESPECIFICACIÓ FUNCIONAL ##

### 1.1. Diagrama de casos d'ús
![](https://dl.dropboxusercontent.com/u/11779029/gps/casos-dus.png)


- Cas d'ús UC001 - *Llistat d'ofertes filtrades*:
L'usuari especificarà una manera de filtrar les ofertes.  
- Cas d'ús UC002 - *Formalitzar compra/venda d'ofertes*:
L'usuari triarà la oferta sobre la qual està interessat i s'obrirà un chat per comunicar-se amb la persona/organització que ha penjat la oferta. L'ofertant podrà acceptar o declinar l'intercanvi. Si s'accepta es realitza el pagament amb moneda-temps.
- Cas d'ús UC003 - *Postejar una oferta*:
L'usuari podrà penjar una oferta al sistema i hi haurà d'introduïr el cost, la localització i la data(en cas de ser una activitat).
- Cas d'ús UC004 - *Esborrar una oferta*:
L'usuari podrà donar de baixa una oferta.
- Cas d'ús UC005 - *Marcar una oferta com a preferida*:
L'usuari podrà marcar una oferta com a preferida.
- Cas d'ús UC006 - *Publicar una review*:
L'usuari podrà publicar una review sobre un altre usuari si han realitzat previament algun intercanvi.
- Cas d'ús UC007 - *Compartir a les xarxes socials*:
L'usuari podrà compartir a les xarxes socials reviews, productes oferts per ell o un altre usuari o els intercanvis que hagi realitzat.
- Cas d'ús UC008 - *Convidar a altres persones a utilitzar Time2Time*:
L'usuari podrà convidar mitjançant un enlaç de referits a altres persones.
- Cas d'ús UC009 - *Aconseguir temps a canvi d'anuncis*:
L'usuari podrà rebre temps a canvi de visualitzar anuncis.
- Cas d'ús UC010 - *Seguir a un usuari*:
L'usuari podrà seguir a altres usuaris, veient les ofertes que han publicat aquestes persones i rebre'n notificacions.
- Cas d'ús UC011 - *Login/registre*:
L'usuari podrà fer login amb el seu nom d'usuari i contrasenya o bé registrar-se. 


### 1.2. Descripció individual dels casos d'ús

#### Cas d'ús UC001 - *Llistat d'ofertes filtrades* ####

>L'usuari establirà un filtratge sobre les ofertes per proximitat, data, categoría o valor de la oferta. El sistema li retornarà un llistat de les ofertes que compleixen les condicions del seu filtratge. 
Excepcions:
	-El rang de dates que ha de posar no pot tenir la data d'inici posterior a la data final. Ambdues dates han de tenir un format vàlid.
	-El valor pel que cerca no pot ser negatiu.

#### Cas d'ús UC002 - *Intercanvi d'ofertes* ####

>L'usuari triarà d'entre un llistat d'ofertes amb un botó sobre quina està interessat. Una vegada clickat el botó s'obrirà un chat amb l'altre persona interessada. En el chat tindrà varies opcions: fer contraofertes de preu, desestimar les ofertes, mostrar que ja no està interessat, aceptar o rebutjar la oferta (en cas de ser-ne l'ofertant).
Excepcions:
	-Les contraofertes no poden tenir valor nul, han de ser números i no poden tenir valors negatius

#### Cas d'ús UC003 - *Postejar una oferta* ####

>L'usuari postejarà una oferta introduint el tipus d'oferta de que es tracta (producte o servei), si és un servei, la categoría que te (social, economic..), el valor/cost que té, una descripció i la possibilitat de penjar fotografies. El sistema recollirà les dades i mitjançant un sistema de geolocalització guardarà les coordenades. Se li ensenyarà al usuari una preview de la oferta que podrà editar, publicar o descartar.
Excepcions:
	-El valor/cost ha de ser un nombre positiu
	-El valor/cost, nom, descripció, categoria i tipus d'oferta no poden ser nul·les

#### Cas d'ús UC004 - *Esborrar una oferta* ####

>L'usuari podrà gestionar les seves ofertes i esborrar-ne les desitjades amb un botó. Una vegada premut se li demanarà confirmació per esborrar l'oferta.
Excepcions:
	-...

#### Cas d'ús UC005 - *Marcar una oferta com a preferida* ####
>L'usuari podrà marcar ofertes o usuaris com a preferits. En cas d'apretar el botó tenint-lo ja com a preferit l'esborrarà de preferits.
Excepcions:
	-...

#### Cas d'ús UC006 - *Publicar una review*: ####
>L'usuari podrà fer reviews sobre usuaris amb els que ha tingut intercanvis o sobre les ofertes d'aquests. Emplenarà un text amb un mínim de 20 caràcters.
Excepcions:
	-Si no ha tingut cap tipus d¡intercanvi amb l'usuari no podrà postejar la review.
	-Si el text no arriba als 20 caracters no podrà postejar la review.

#### Cas d'ús UC007 - *Compartir a les xarxes socials* ####
>L'usuari podrà compartir a les xarxes socials mitjançant un botó ofertes o usuaris.
Excepcions:
	-...

#### Cas d'ús UC008 - *Convidar a altres persones a utilitzar Time2Time* ####
>L'usuari podrà convidar a altres persones a utilitzar Time2Time mitjançant un enllaç de referits.
Excepcions:
	-...

#### Cas d'ús UC009 - *Aconseguir temps a canvi d'anuncis* ####
>L'usuari visualitzarà un cert nombre/temps d'anuncis per poder aconseguir temps. En qualsevol moment podrà sortir del mode anuncis.
Excepcions:
	-Si no compleix el temps/nombre d'anuncis que ha de visualitzar no conseguirà temps a canvi.

#### Cas d'ús UC010 - *Seguir a un usuari* ####
>Podrà seguir a altres usuaris mitjançant un botó. D'aquesta manera rebrà notificacions sobre l'activitat pública d'altres usuaris.
Excepcions:
	-No es pot seguir més d'una vegada a un altre usuari.
	-No es pot seguir a ell mateix.

#### Cas d'ús UC011 - *Login/registre* ####
L'usuari podrà fer login amb el seu nom d'usuari i contrasenya Si aquests ddades són correctes s'accedirà al seu perfil i podrà comprar les ofertes. En cas de que no sigui correcte se li demanarà que torni a entrar les dades. També se li donarà la opció, en cas de que no tingui usuari, de que es registri. L'usuari introduirà les dades necessaries pel registre, aceptarà les condicions i enviarà el formulari. Un cop validat el formulari s'afegirà a la base de dades.

### Mockups ###
![](https://dl.dropboxusercontent.com/u/11779029/gps/wireframes.png)




## 2. ESPECIFICACIÓ NO FUNCIONAL ##

> Descriure en més detall els requisits no funcionals que han sortit al document de visió, intentant fer-los el més quantificables possible

*Rendiment del sistema:
El sistema ha de poder funcionar sota uns requisits de hardware baixos sense deteriorar en excés la rapidesa del sistema.
*Escalabilitat del sistema:
El sistema ha de ser capaç d'absorbir pics de demanda i poder creixer tant en usuaris com ofertes sense que el rendiment se'n vegi afectat.
*Usabilitat del sistema:
El sistema ha d'oferir una interficie amigable i fàcil d'utilitzar, on tot quedi ben detallat.
*Connectivitat
El sistema ha d'oferir sistemes per comunicar-se amb altres.
*Seguretat
El sistema ha de ser robust i segur per assegurar la privacitat i les monedes virtuals dels usuaris.