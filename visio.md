# SISTEMA ACME - VISIÓ #

Time4Time és la pròxima aplicació mòbil destinada a intercanviar temps per activitats o objectes. A través d'aquesta aplicació els usuaris posaran a disposició d'altres usuaris objectes i activitats de diferent tipus per guanyar temps. Un temps que haurà marcat el mateix usuari. Quan els altres usuaris entrin a l'aplicació podran veure les diferents ofertes que altres usuaris hauran penjat. Els usuaris podran adquirir els serveis de l'aplicació només si al seu rellotge hi tenen suficient temps. Mai ningú podrà superar un màxim de temps ni rebaixar un mínim, així el temps estarà constantment en moviment.


## 1. INTRODUCCIÓ ##
Time4Time és una aplicació mòbil destinada a intercanviar temps per activitats o objectes. Els usuaris faran ofertes de diferent tipus per acumular temps. Un temps que haurà marcat el mateix usuari. Els altres usuaris podran veure les diferents ofertes que hi ha disponibles, i les podran adquirir només si acumulen suficient temps.


## 2. EL PROBLEMA ##
La solució és clara, el lligam entre els usuaris que tenen temps però no recursos, i els usuaris que tenen recursos però no tenen temps. Un win-to-win on els usuaris participaran per obtenir el que desitgin.


## 3. PARTS INTERESSADES ##
L'aplicació té dos parts principals interessades:

La Miriam és aquella persona que es passa la vida intentant aconseguir recursos, i cada cop destina més temps a la seva feina. Porta una vida plena de responsabilitats i obligacions. El factor emocional de la Miriam és que ella sempre ha volgut realitzar un seguit d'activitats que mai no va tenir oportunitat de fer. Coses de caràcter més alternatiu que potser són difícils d'aconseguir amb diners. Ella veu una oportunitat per fer un canvi i afegir valor a la seva vida.

El Marc és aquella persona que disposa d'una quantitat de temps lliure considerable, els seus recursos són més limitats que els de la Miriam. Però ell té coneixements que són de gran interès per la Miriam.


## 4. EL PRODUCTE ##
Així l'objectiu principal del projecte és connectar aquestes parts interessades. Gestionarà les ofertes que els usuaris vagin penjant a la plataforma.


### 4.1. Perspectiva del producte ###
El producte seria una aplicació mòbil per iOS, Android i plataforma web amb el seu propi mercat. Els usuaris que publiquin les ofertes seran qui decidiran el temps que costa la seva oferta, el lloc, la durada i la data. Així que l'única cosa que necessita Time4Time és un servidor potent.

![](https://dl.dropboxusercontent.com/u/11779029/gps/arquitectura-servidors.png)


### 4.2. Descripció del producte ###
1. Els usuaris han de poder donar d'alta i de baixa les seves ofertes.
2. Els usuaris han de poder visualitzar les ofertes disponibles de l'aplicació.
3. Ha de permetre que els usuaris marquin una oferta que resulti del seu interés.
4. El producte ha de permetre confirmar l'intercanvi entre les parts interessades.
5. Si és així, també ha d'actualitzar el temps acumulat de cada un.
6. Els usuaris també poden cancel·lar una reserva.
7. Escriptura d'un review del producte i l'usuari.
8. Compartir les seves experiències a les xarxes socials.
9. Invitar a més persones a descarregar l'aplicació directament des de l'aplicació.


### 4.3. Supòsits de funcionament ###
Per poder utilitzar l'aplicació els usuaris hauran de:

1. Disposar d'un perfil a la plataforma.
2. Disposar de connexió a internet.
3. El valor de cada oferta serà determinat per l'usuari de manera raonable, segons l'oferta, la demanda i la pròpia autoregulació.


### 4.4. Dependències sobre altres sistemes ###
L'aplicació dependrà dels sistemes de Phonegap per desenvolupar i mantenir l'app. També dependrà dels servidors on estigui allotjada


### 4.5. Altres requisits ###
L'aplicació ha de ser ràpida i vistosa, ha de despertar el factor emocional dels usuaris per participar en aquestes activitats.
L'aplicació ha de tenir accés a la posició dels usuaris per poder filtrar les ofertes


## 5. RECURSOS ##
Time4Time utilitzarà els serveis Cloud d'Amazon per disminuir el temps de producció, augmentar la disponibilitat del sistema i l'escalabilitat
Time4Time necessitarà recursos econòmics per realitzar una campanya de màrqueting i manteniment de l'aplicació.