# SISTEMA ACME - RISCS #

## RISC 001. Donar a conèixer l'aplicació. ##

### Descripció ###
Que no es dongui suficientment a conèixer l'aplicació i per tant que quasi no hi hagi oferta de classes o de productes i, per tant, que la gent acabés no utilitzant-la per falta de dades.

### Probabilitat ###
Probable si no s'estudia bé com s'enfoca al principi

### Impacte ###
Si l'aplicació no es dona suficientment a conèixer o la gent no ofereix les seves hores, no es donaria a conèixer perque no hi hauria oferta/demanda i per tant, acabaria fent que ningú utilizés l'aplicació.

### Indicadors ###
- Que la tassa de conversió de la gent que es baixa l'aplicació sigui molt petit o casi nul.
- El nombre de classes que la gent ofereixi
- El nombre de productes que la gent puji
- Que el nombre d'usuaris que es baixen l'aplicació sigui molt petit.

### Estratègies de mitigació ###
Enfocar el màxim de bé com plantejar l'inici del projecte. És a dir parlar amb gent que tingui temps i penji les seves classes o gent fer un acord amb wallapop per oferir el canvi de productes per hores, entre d'altres.

### Plans de mitigació ###
- Buscar bloggers, youtubers, etc. que donin a conèixer la plataforma
- Oferir hores gratuites per a tothom quan es registren.



## RISC 002. Generació de frau. ##

### Descripció ###
Que hi hagi gent que faci bots o programes o, fins i tot ells mateixos, generant falses ofertes o anuncis que no pertoquin.

### Probabilitat ###
Versemblant. Tot depen de la "seguretat" que fem que tingui l'aplicació.

### Impacte ###
General. Ho veuria tothom que utilitzés l'aplicació i mirés les ofertes disponibles.

### Indicadors ###
- Que hi hagi una persona del nostre equip que revisi de tant en tant les ofertes.
- Que els mateixos clients/gent que fa servir l'aplicació, reporti l'anunci/s que són fradulents.

### Estratègies de mitigació ###
Enfocar el màxim de bé com plantejar la seguretat del projecte:
- Posar captchas (No sóc un robot) o fer comprovacions de l'estil SMS
- Una vegada es penji un anunci, comprovar que sigui adient havent-lo d'acceptar per un treballador del nostre equip

### Plans de mitigació ###
- Aplicar el que hem comentat dels captches i la comprovació dels anuncis
- Afegir un botó per reportar anuncis fradulents



## RISC 003. Comunicació errònea dels stakeholders. ##

### Descripció ###
Que no quedin clares les especificacions per a l'usuari o no acabi d'entendre per a que serveix o es pensi que serveix per una altra cosa.

### Probabilitat ###
Probable si no s'estudia bé com s'enfoca al principi i com es fa per arribar als usuaris com funciona i per a que serveix l'aplicació.

### Impacte ###
Afecta a tota l'aplicació i projecte en general. Si els usuaris no ho tenen clar, no tindrà èxit.

### Indicadors ###
- No s'aconsegueixin usuaris
- L'aplicació no té anuncis ni visites

### Estratègies de mitigació ###
Enfocar el màxim de bé com plantejar l'inici del projecte. Tenir molt clar per a que serveix i saber-ho transmetre als usuaris.

### Plans de mitigació ###
Reenfocar-ho i que la gent vegi que no és el que pensava que era, sinó el que realment és i per a que s'utilitza.



## RISC 004. No deixar-lo abandonat. ##

### Descripció ###
Que no quedi l'aplicació amb les funcionalitats essencials abandonada, sinó que hi hagi millores constants i afegir noves funcionalitats


### Probabilitat ###
Poc probable si es fa com es vol. És a dir, si no s'abandona i està en constant millora i llegint les valoracions dels clients i estan pendent del projecte, hauria d'anar bé i que no passés aquest risc

### Impacte ###
Que s'aconsegueixin clients i al no actualitzar-la o arreglar els errors comporti perdre'ls.

### Indicadors ###
- Pèrdua de clients
- Pèrdua de visites de l'aplicació
- Menys guany de descàrregues

### Estratègies de mitigació ###
Com s'ha comentat anteriorment, es pot reduïr llançant actualitzacions setmanals, arreglant errors i afegint noves funcionalitats tenint en compte les valoracions (reviews-feedback) dels usuaris.

### Plans de mitigació ###
Ser constant i busca sempre la millora de l'aplicació i tenir en compte el que els usuaris pensen i diuen.



## RISC 005. El projecte es queda sense líquid d'inversió inicial per dur-lo a terme. ##

### Descripció ###
Que els diners de la persona o empresa que ha invertit amb nosaltres s'acabin abans de lo previst.

### Probabilitat ###
Versemblant, depenent de la inversió i dels càlculs que s'hagin fet de pressupostos i de com vagi avançant el projecte

### Impacte ###
Alt en el sentit que s'haurien d'invertir diners per part de les parts interessades o demanar més diners a l'inversor, cosa que sigui molt probable que digui que no.

### Indicadors ###
Doncs que a mesura que avança el projecte, es pot fer un seguiment dels diners invertits (a nivell pràctic i teòric) i si s'allunyen molt, pot ser un indicador que algo no va bé i, per tant, que el projecte es quedi sense diners.

### Estratègies de mitigació ###
Fer un seguiment constant dels diners invertits reals amb els teòrics fets quan es va proposar a l'inversor.

### Plans de mitigació ###
En cas de gastar-se més del previst en algun punt, retallar en d'altres que no siguin indispensables o puguin ser "retallats" per tal de poder recuperar la inversió extra que s'hagi fet.



## RISC 006. La tecnologia emprada deixi de donar suport. ##

### Descripció ###
Les tecnologies que es fan servir per desenvolupar l'aplicació, deixin de donar suport a aquestes o als programes o frameworks utilitzats.

### Probabilitat ###
Poca, ja que pot passar però no és lo normal amb tecnologies molt conegudes com les que utilitzarem.

### Impacte ###
Alt en el sentit que s'hauria de programar tot de nou o que deixaria de ser actualitzable (afegir noves funcionalitats, ...)

### Indicadors ###
Que l'empresa o llenguatge passi a estar obsoleta o desapareixi.

### Estratègies de mitigació ###
Intentar preveure si el framework/l'empresa/llenguatge s'està quedan desactualitzat o obsolet abans que ho estigui del tot'

### Plans de mitigació ###
Tornar a programar tota l'aplicació amb el sistema/framework/empresa que substitueixi a l'obsoleta




## RISC 007. Bombolla econòmica de la moneda-temps ##

### Descripció ###
Quan un usuari nou es registre, Time4Time dona unes monedes virtuals de prova, i aquestes tenen un valor. Si el valor de la moneda-temps puja de cop de manera global o baixa, hi haurà un problema de canvi de moneda. És a dir, com el canvi d'euro a dòlar puja i baixa constantment, aquí, encara que no es pugui fer un canvi directe euro a moneda-temps, pot passar que la moneda es desvaloritzi o tot el contrari, puji de valor.

### Probabilitat ###
Probable, pot passar que puji o baixi en qualsevol moment. En principi no hauria de ser lo normal, però podria passar sense cap problema.

### Impacte ###
Mitjà-Alt, ja que tothom hauria de pujar els preus de cop o baixar-los per tal de regular l'oferta i la demanda.

### Indicadors ###
Es podria veure amb el preu mig de monedes-temps dels anuncis dels usuaris. Sinó, es podria mirar l'historial de totes les classes o objectes penjats si han sigut modificats el preu que es demana o s'ofereix per aquests.

### Estratègies de mitigació ###
El sistema s'autorregularia per oferta i demanda. I les monedes-temps que es donen als usuaris nous, s'haurian d'incrementar o decrementar segons el valor que tingui en aquell moment. És a dir, en comptes de donar 5 monedes-temps, donar un 0,05% que equival a 2,3,5,10 monedes-temps depenen en com estigui el suposat canvi de moneda-temps euro.

### Plans de mitigació ###
Posar un màxim de diners als usuaris, és a dir que si tenen moltes monedes-temps, tinguin un límit per fer-les servir o com a mínim que utilitzin unes quantes per no tenir-ne tantes.





## RISC 008. Un usuari es creï diversos comptes ##

### Descripció ###
Que hi hagi gent que faci bots o programes o, fins i tot ells mateixos, intentant crear comptes falses per tenir monedes-temps gratuites.

### Probabilitat ###
Versemblant. Tot depen de la "seguretat" que fem que tingui l'aplicació.

### Impacte ###
Alt. Passaria desaparcebut però provocaria el Risc 007, que la moneda es revaloritzés ja que no pararia d'entrar moneda-temps i podria acabar en que la gent no hi confiés i deixés d'utilitzar Time4Time.

### Indicadors ###
- Que hi hagi una persona del nostre equip que revisi de tant en tant els comptes i que no hi hagi coincidències de correus, telèfons, noms o IP's de connexió.
- Que els mateixos clients/usuaris que fa servir l'aplicació, pugui reportar als usuaris que semblin fradulents.

### Estratègies de mitigació ###
Enfocar el màxim de bé com plantejar la seguretat del projecte:
- Posar captchas (No sóc un robot) o fer comprovacions de l'estil SMS o que sigui obligatori posar un telèfon mòbil, confirmar-lo i que sigui únic per usuari. Fins i tot, escanejar el DNI per la màxima seguretat.
- Una vegada es registre un usuari, es demani que confirmi altres comptes com Facebook, Twitter, Gmail, ... i si ho fan se'ls hi donen monedes-temps com a recompensa.

### Plans de mitigació ###
- Aplicar el que hem comentat dels captches, SMS, telèfon mòbil, comprovació de Gmail, Facebook...
- Afegir un botó per reportar usuaris fradulents
- Fer un programa intern que revisi la base de dades una vegada al dia amb tots els registres nous i els compari amb els ja existents per intentar detectar si hi ha nous usuaris que puguin ser fradulents.


